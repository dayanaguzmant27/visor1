function initMap(){
    var mymap = L.map('visor').setView([4.743938, -74.025741], 15);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZGF5YW5hZ3V6bWFuMjciLCJhIjoiY2tjcXdqaGc4MTk2cDJxczVraWJseDkycSJ9.eXyrA0Xwby8LV8k6gaty1w'
    }).addTo(mymap);
    
    var markers=[
        ["Panadería",4.743255, -74.029611],
        ["SimonBolivar",4.658437, -74.093454],
        ["ParqueelSalitre",4.663559, -74.087126],
        ["Tercermilenio",4.597909, -74.081773]
    ];

    for (var i = 0; i < markers.length; i++) {
        marker = new L.marker([markers[i][1],markers[i][2]]).addTo(mymap);
      }

    markers[2].bindpopup("panaderia");

}